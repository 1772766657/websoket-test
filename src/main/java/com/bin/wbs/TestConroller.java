package com.bin.wbs;

import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Controller
public class TestConroller {
    @Autowired
    private SimpMessageSendingOperations simpMessageSendingOperations;
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    public final static String INSERT_INTO_DATA_TEST = "INSERT INTO  DATA_TEST VALUES(?,?,?,?)";
    public final static String CREATE_TABLE_DATA_TEST = "CREATE TABLE IF NOT EXISTS DATA_TEST(ID INT  PRIMARY KEY,KEY VARCHAR(200),MSG VARCHAR(1000),STATE VARCHAR(100))";
    //测试方法
    @RequestMapping("/test")
    @ResponseBody
    public void sendMessage(Integer num)
    {   SqlExecutor.updateBySql(CREATE_TABLE_DATA_TEST);
        ActiveMQTopic destination = new ActiveMQTopic("my_msg");
        String s=SqlExecutor.queryBySql("SELECT COUNT(*) FROM DATA_TEST").get(0).get("COUNT(*)").toString();
        int index=Integer.parseInt(s);

        ExecutorService executorService= Executors.newFixedThreadPool(100);
        for(int i=index;i<index+num;i++) {
            final int temp = i;
            executorService.execute(new Runnable() {
                @Override
                public void run() {

                    SqlExecutor.updateBySql(INSERT_INTO_DATA_TEST, temp, UUID.randomUUID().toString(), System.currentTimeMillis(), 0);
                    jmsMessagingTemplate.convertAndSend(destination, "this is msg" + temp);
                }
            });

        }
    }


    @MessageMapping("/marco")
    public void handleShout(Shout incoming) 
    {
	System.out.println("Received message:"+incoming.getMessage());
    }
    
    @SubscribeMapping("/subscribe")
    public Shout handleSubscribe() 
    {
	Shout  outing = new Shout();
	outing.setMessage("subscribes");
	return outing;
    }
}
