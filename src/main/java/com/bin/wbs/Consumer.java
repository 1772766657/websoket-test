package com.bin.wbs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerContainerFactory;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

import javax.jms.ConnectionFactory;
import java.util.List;
import java.util.Map;

@Component
public class Consumer {
    @Autowired
    private SimpMessageSendingOperations simpMessageSendingOperations;

    @JmsListener(destination = "my_msg",containerFactory = "myJmsContainerFactory")
    public void readMsg(String text) {
        if(text.contains("this is msg")){
           String id=text.replace("this is msg","");
           List<Map<String,Object>> list=SqlExecutor.queryBySql("SELECT * FROM DATA_TEST WHERE ID = ?",Integer.parseInt(id.trim()));
           simpMessageSendingOperations.convertAndSend("/topic/test",list.get(0));
        }
        System.out.println("接收到消息：" + text);
    }
    @Bean
    JmsListenerContainerFactory<?> myJmsContainerFactory(ConnectionFactory connectionFactory){
        SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setPubSubDomain(true);
        return factory;
    }

}