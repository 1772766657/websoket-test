package com.bin.wbs;

import org.apache.activemq.command.ActiveMQTopic;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



@RunWith(SpringRunner.class)
@SpringBootTest
public class SqlExecutorTest {
    public final static String CREATE_TABLE_DATA_TEST = "CREATE TABLE IF NOT EXISTS DATA_TEST(ID INT  PRIMARY KEY,KEY VARCHAR(200),MSG VARCHAR(1000),STATE VARCHAR(100))";
    public final static String INSERT_INTO_DATA_TEST = "INSERT INTO  DATA_TEST VALUES(?,?,?,?)";
    public final static String QUERY_DATA_TEST="SELECT * FROM DATA_TEST WHERE ID = ?";
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Test
    public void updateBySql() throws Exception {
       SqlExecutor.updateBySql(CREATE_TABLE_DATA_TEST);
      /*  for (int i = 2000; i < 20000; i++) {
            String uid = UUID.randomUUID().toString();
            String msg = "MSG-[" + UUID.randomUUID().toString() + "]";
            SqlExecutor.updateBySql(INSERT_INTO_DATA_TEST, i, uid, msg, i % 4);
        }*/
    }

    //数据库压力测试
    //time all:540 time max:402 time average:25
    //time all:580 time max:388 time average:28
    @Test
    public void queryBySql() throws Exception {
        CountDownLatch latch = new CountDownLatch(2000);
        long start = System.currentTimeMillis();
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        List<Integer> list= Collections.synchronizedList(new ArrayList<>());

        for (int i = 0; i < 2000; i++) {
            final int temp=i;
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    long startQuery=System.currentTimeMillis();
                    SqlExecutor.queryBySql(QUERY_DATA_TEST,temp);
                    long endQuery=System.currentTimeMillis();
                    int spend= (int) (endQuery-startQuery);
                    list.add(spend);
                    latch.countDown();

                }
            });
        }
        latch.await();
        long end=System.currentTimeMillis();
        Assert.assertTrue(list.size()==2000);

        int max=0;
        int sum=0;
        for (Integer integer : list) {
            max=max>integer?max:integer;
            sum+=integer;
        }
        int average=sum/list.size();
        System.out.println("time all:"+(end-start));
        System.out.println("time max:"+max);
        System.out.println("time average:"+average);
    }

    @Test
    //time all:5989 time max:1076 time average:296
    //time all:4133 time max:568  time average:202
    public   void TestMq() throws  Exception{
        ActiveMQTopic destination = new ActiveMQTopic("my_msg");
        ExecutorService executorService=Executors.newFixedThreadPool(100);
        CountDownLatch countDownLatch=new CountDownLatch(2000);
        long start=System.currentTimeMillis();
        List<Integer> list= Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < 2000; i++) {
            final int temp=i;
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    long startSend=System.currentTimeMillis();
                    jmsMessagingTemplate.convertAndSend(destination,"this is msg"+temp);
                    long endSend=System.currentTimeMillis();
                    int spend= (int) (endSend-startSend);
                    list.add(spend);
                    countDownLatch.countDown();
                }

            });

        }
        countDownLatch.await();
        long end=System.currentTimeMillis();
        int max=0;
        int sum=0;
        for (Integer integer : list) {
            max=max>integer?max:integer;
            sum+=integer;
        }
        int average=sum/list.size();
        System.out.println("time all:"+(end-start));
        System.out.println("time max:"+max);
        System.out.println("time average:"+average);

    }


@Test
public void testPush2Web() throws Exception{

}

}